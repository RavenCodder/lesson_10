package com.example.lesson10

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


//===========================Buttons for image and text visibility =================================

        img_btn.setOnClickListener {
            if (img_for_btn.visibility == View.INVISIBLE) {
                img_for_btn.visibility = View.VISIBLE
            } else {
                img_for_btn.visibility = View.INVISIBLE
            }
        }


        text_btn.setOnClickListener {
            if (tv_for_btn.visibility == View.INVISIBLE) {
                tv_for_btn.visibility = View.VISIBLE
            } else {
                tv_for_btn.visibility = View.INVISIBLE
            }
        }
//==================================================================================================

//=========================== Saving data to log  ==================================================


        reg_btn.setOnClickListener {
            Log.d("Reg", "Name: " + name_et.text.toString() )
            Log.d("Reg", "Surname: " + surname_et.text.toString() )
            Log.d("Reg", "Email: " + mail_et.text.toString() )
            Log.d("Reg", "Password: " + password_et.text.toString() )
        }
//==================================================================================================
    }
}
